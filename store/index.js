import Vuex from 'vuex'

const createStore = () => {
  return new Vuex.Store({
    state: {
      counter: 0,
      nav: false
    },
    mutations: {
      increment (state) {
        state.counter++
      },
      toggleNav (state) {
        state.nav = !state.nav
      }
    }
  })
}

export default createStore
